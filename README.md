# Build Open API

Red Hat Building Open API with Agile Integration workshop

>  Workshop URL:  https://tutorial-web-app-webapp.apps.bkkbp-17d3.open.redhat.com


## Lab 01 - Backend Service

Use follow OpenAPI specification to create API Provider:

> https://gitlab.com/ocp-demo/open-banking/raw/master/open-data-apis-nokey.json 



## Lab 02 - Managing API Endpoints

if you get following error message, when new API by import from OpenShift

`Something is not quite ok. Is your private API host reachable from the API gateway? `

please new API manually [click](3scale/private_api_unreachable.pdf)


## OPEN (Online Partner Network Enablement)

[https://www.redhat.com/en/partners/course_catalog](https://www.redhat.com/en/partners/course_catalog)

### Learning Path

**Red Hat Delivery Specialist - Enterprise Integration**
* Red Hat Fuse 7 Foundations Part 1: Fuse Online
* Red Hat Fuse 7 Foundations Part 2: Camel
* Camel Development with Red Hat Fuse 7 Part 1
* Introduction to Red Hat Fuse 7 on OpenShift


**Red Hat Delivery Specialist - Agile Integration**
* Red Hat 3scale API Management Platform Foundations
* Red Hat Fuse 7 Foundations Part 1: Fuse Online
* Red Hat Fuse 7 Foundations Part 2: Camel
* Red Hat AMQ 7 Foundations
* Introduction to Red Hat Fuse 7 on OpenShift


**Red Hat Delivery Specialist - API Management**
* Red Hat 3scale API Management Platform Foundations
* Red Hat 3scale API Management Implementation
* Red Hat 3scale API Management Development


**Red Hat Delivery Specialist - Container Platform Application Deployment (OCP 4)**
* Red Hat OpenShift 4 Foundations
* Red Hat OpenShift Container Platform 4 Resources and Tools
* Application Deployment with Red Hat OpenShift Container Platform 4


**Red Hat Delivery Specialist - Container Platform Deployment (OCP 4)**
* Red Hat OpenShift 4 Foundations
* Red Hat OpenShift Container Platform 4 Resources and Tools
* Red Hat OpenShift Container Platform 4 Installation
* Red Hat OpenShift Container Platform 4 Configuration



